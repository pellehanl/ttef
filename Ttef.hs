module Ttef where
type Row = [Int]
type Field = [[Int]]
type Score = Int
type Game = (Field, Score)

a :: Row
a = [1,0,1,5]
b :: Field
b = [a,a,a,a]

mrl :: Row -> Row
mrl f = mt $ shift 1 (rmz f) where
    mt [] = []
    mt (x:xs) = if uncurry (==) x && fst x /= 0 then fst x + 1 : mt (tail xs) else fst x : mt xs
    shift n x = zip x $ drop n (x ++ [0])
    rmz = filter (/= (0::Int))

(<~>) :: (a -> b) -> (b -> a) -> a -> b
f <~> g = f . g . f

replace :: [a] -> Int -> a -> [a]
replace l n v = take n l ++ [v] ++ drop (n + 1) l

putVal :: Field -> Int -> Int -> Int -> Field
putVal f x y v = replace f x (replace (f!!x) y v)

renormB :: Row -> Row
renormB f = replicate (4-length f) 0 ++ f

renormE :: Row -> Row
renormE f = f ++ replicate (4 - length f) 0

mergeLeft :: Field -> Field
mergeLeft = map (renormE . mrl)

transpose :: Field -> Field
transpose ([]:_) = []
transpose x = map head x : transpose (map tail x)

mergeRight :: Field -> Field
mergeRight = map (renormB . mrl)

mergeUp :: Field -> Field
mergeUp = transpose <~> mergeLeft

mergeDown :: Field -> Field
mergeDown = transpose  <~> (map reverse. mergeLeft)

countZeros :: Row -> Int
countZeros r = sum [1 | x <- r, x == 0] :: Int

countZerosField :: Field -> Int
countZerosField = sum . map countZeros

tileScore :: Int -> Int
tileScore t = t * round (logBase 2 (fromIntegral t)::Double)

score :: Field -> Int
score f = sum (map (sum .map tileScore)f)

isOver :: Field -> Bool
isOver f = 0 == sum ( map countZeros f) && cscore == foldl max cscore ( map score [mergeLeft f, mergeRight f, mergeUp f, mergeDown f]) where
    cscore = score f

