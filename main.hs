import System.Random
import Ttef

z :: Field
z = [[0,0,0,0], [0,0,0,0], [0,0,0,0], [0,0,0,0]]

putRandomTwo :: Field -> IO Field
putRandomTwo f = do
    xx <- randomIO :: IO Int
    yy <- randomIO :: IO Int
    let x = xx `mod` 4
    let y = yy `mod` 4
    print (x,y)
    if 0 == f!!x!!y then return $ putVal f x y 1 else putRandomTwo f

printField :: Field -> IO ()
printField f =  putStr $ concat $ zipWith (++) (map showRow f) (replicate 4 "\n") where
    showRow v = concat $ zipWith (++) (map show v) (replicate 4 " ")

playGame :: Field -> IO ()
playGame f = do
    printField f
    key <- getLine
    g <- putRandomTwo f
    --printField g
    case key of
        "a" -> playGame $ mergeLeft g
        "s" -> playGame $ mergeDown g
        "d" -> playGame $ mergeRight g
        "w" -> playGame $ mergeUp g
        "p" -> return ()
        _ -> playGame f

main :: IO ()
main = playGame z

